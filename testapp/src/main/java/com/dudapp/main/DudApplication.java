package com.dudapp.main;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.dudapp.controller.**"})
public class DudApplication {

    public static void main(String[] args){
        SpringApplication.run(DudApplication.class, args);
    }
}
