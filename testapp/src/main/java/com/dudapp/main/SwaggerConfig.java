package com.dudapp.main;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {                                    
    @Bean
    public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2)  
          .select()                                  
          .apis(RequestHandlerSelectors.basePackage("com.dudapp.controller.**"))
          .paths(PathSelectors.any())                          
          .build().apiInfo(apiInfo());                                           
    }
    
    private ApiInfo apiInfo() {
    	Contact c = new Contact("Dud application", "", "");
        return new ApiInfo("Dud application", "This an api used to prove cf push works",
                null, null, c, null, null);
    }
}
