package com.dudapp.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @RequestMapping("/")
    String home(){
        return "<h1>'allo, 'allo, 'allo</h1>";
    }

    @RequestMapping("/ldap")
    String getLdap(){
        return "ldap";
    }
}
